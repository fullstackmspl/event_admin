import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalyticsComponent } from './analytics/analytics.component';
import { DashboardComponent } from './dashboard.component';
import { EventlistComponent } from './eventlist/eventlist.component';

const routes: Routes = [{
  path: '', component: DashboardComponent,
  children: [
    // { path: '', pathMatch: 'full', redirectTo: 'dashboard', },
    { path: 'analytics', component: AnalyticsComponent },
    { path: 'event_List', component: EventlistComponent }
  ]
},
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
