import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  analytics(){
    this.router.navigate(['/dashboard/analytics'])

  }
  eventList(){
    this.router.navigate(['/dashboard/event_List'])
  }
}
